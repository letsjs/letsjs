<?php namespace App;

use Illuminate\Database\Eloquent\Model;


class Member extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'members';


	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['first_name', 'last_name', 'user_id'];

	public function posts() {
		return $this->hasMany('App\Post');
	}

	public function members() {
		return $this->belongsTo('App\User');
	}

}
