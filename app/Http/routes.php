<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

use Illuminate\Http\Response as HttpResponse;

Route::get('/', function(){

	if (Auth::check()) {
		return response()->view('myhome');
	}
	return response()->view('auth.login');
});

Route::get('home', 'HomeController@index');

Route::get('whoami', 'JWTAuthController@getAuthenticatedUser');

Route::resource('members', 'MemberController');

Route::resource('members.posts', 'PostController');



Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

Route::post('/signup', 'JWTAuthController@signUp');

Route::post('/signin', function () {
   $credentials = Input::only('email', 'password');

   if ( ! $token = JWTAuth::attempt($credentials)) {
       return Response::json(false, HttpResponse::HTTP_UNAUTHORIZED);
   }

   return Response::json(compact('token'));
});