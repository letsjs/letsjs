<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Post;
use Input;
use Response;


use Illuminate\Http\Request;

class PostController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($member)
	{
		return Post::where('member_id','=',$member)->get();
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		if (Input::has('subject','body','member_id')) {
			$input = Input::all();
			if ($input['subject'] == '' || $input['body'] == '' || $input['member_id'] == '') {
				return Response::make('Fill all the fields', 400);
			}

			$post = new Post;
			$post->subject = $input['subject'];
			$post->body = $input['body'];
			$post->member_id = $input['member_id'];
			$post->save();

			return $post;
		} else {
			return Response::make('Provide all the fields', 400);
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($member, $post)
	{
		return Post::where('member_id','=',$member)->where('id','=',$post)->get();
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($post)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($post)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($post)
	{
		$find_post = Post::find($post);
		$find_post->delete();
		return $post;
	}

}
