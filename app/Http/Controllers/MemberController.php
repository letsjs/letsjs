<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Member;
use Input;

use Illuminate\Http\Request;
use Response;

class MemberController extends Controller {

	public function __construct()
	{
	    $this->middleware('jwt.auth', ['only' => ['index']]);  // or use 'only' in place of except
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return Member::all();
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('newmember');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		if (Input::has('first_name','last_name','user_id')) {
			$input = Input::all();
			if ($input['first_name'] == '' || $input['last_name'] == '') {
				return Response::make('Fill all the fields', 400);
			}

			$member = new Member;
			$member->first_name = $input['first_name'];
			$member->last_name = $input['last_name'];
			$member->user_id = $input['user_id'];			
			$member->save();

			return $member;
		} else {
			return Response::make('Provide all the fields', 400);
		}

		return "Hello";
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($member)
	{
		return Member::find($member);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	// public function edit($id)
	// {
	// 	//
	// }

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	// public function update($id)
	// {
	// 	//
	// }

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($member)
	{
		$find_member = Member::find($member);
		$find_member->delete();
		return $member;
	}

}
