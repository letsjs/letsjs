<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Member;
use Input;
use Hash;
use JWTAuth;
use Response;

use App\Http\Requests;

class JWTAuthController extends Controller
{
    public function __construct()
	{
	    $this->middleware('jwt.auth', ['except' => ['signUp']]);  // or use 'only' in place of except
	}

	public function index()
    {
        return response()->json(['auth'=>Auth::user(), 'users'=>User::all()]);
    }

    public function getAuthenticatedUser()
	{
	    try {

	        if (! $user = \JWTAuth::parseToken()->authenticate()) {
	            return response()->json(['user_not_found'], 404);
	        }

	    } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

	        return response()->json(['token_expired'], $e->getStatusCode());

	    } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

	        return response()->json(['token_invalid'], $e->getStatusCode());

	    } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

	        return response()->json(['token_absent'], $e->getStatusCode());

	    }

	    // the token is valid and we have found the user via the sub claim
	    return response()->json(compact('user'));
	}

	public function signUp()
	{
		// Create hashed password
	   $hashedpass = Hash::make(Input::get('password'));   
	   $credentials = ['email' => Input::get('email'), 'password' => $hashedpass];

	   try {
	       $user = User::create($credentials);
	   } catch (Exception $e) {
	       return Response::json(['error' => 'User already exists.'], HttpResponse::HTTP_CONFLICT);
	   }
	   
	   $memberDetails = ['first_name' => Input::get('first_name'), 'last_name' => Input::get('last_name'), 'user_id' => $user->id];

	   $member = Member::create($memberDetails);

	   $token = JWTAuth::fromUser($user);

	   return Response::json(compact('token'));
	}
}
