<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('users')->delete();

		$users = array(
			['id' => 1, 'email' => 'test1@lets.com', 'password' => Hash::make('test1'), 'created_at' => new DateTime, 'updated_at' => new DateTime],
			['id' => 2, 'email' => 'test2@lets.com', 'password' => Hash::make('test2'), 'created_at' => new DateTime, 'updated_at' => new DateTime],
			['id' => 3, 'email' => 'test3@lets.com', 'password' => Hash::make('test3'), 'created_at' => new DateTime, 'updated_at' => new DateTime],
			['id' => 4, 'email' => 'test4@lets.com', 'password' => Hash::make('test4'), 'created_at' => new DateTime, 'updated_at' => new DateTime],
			['id' => 5, 'email' => 'test5@lets.com', 'password' => Hash::make('test5'), 'created_at' => new DateTime, 'updated_at' => new DateTime],
			);

		DB::table('users')->insert($users);

	}

}

