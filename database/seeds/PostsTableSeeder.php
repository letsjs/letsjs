<?php

use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('posts')->delete();

		$posts = array(
			['id' => 1,'subject' => 'Revenge', 'body' => 'Every time a bird poops on my car, I eat a plate of scrambled eggs on my porch, just to show them what I am capable of.', 'member_id' => 1, 'created_at' => new DateTime, 'updated_at' => new DateTime],
			['id' => 2,'subject' => 'Nosy', 'body' => 'On a scale of Voldemort to Pinocchio, how Nosy are you?', 'member_id' => 2, 'created_at' => new DateTime, 'updated_at' => new DateTime],
			['id' => 3,'subject' => 'Mr. Lee', 'body' => 'You remind me of my Asian friend... Ug Lee.', 'member_id' => 4, 'created_at' => new DateTime, 'updated_at' => new DateTime],
			['id' => 4,'subject' => 'Infallible', 'body' => 'I\'m never wrong. I once thought I was wrong, turns out, I was mistaken.', 'member_id' => 1, 'created_at' => new DateTime, 'updated_at' => new DateTime],
			['id' => 5,'subject' => 'Hyper Text Coffee Pot Control Protocol (HTCPCP/1.0) ', 'body' => 'There is coffee all over the world. Increasingly, in a world in which computing is ubiquitous, the computists want to make coffee. Coffee brewing is an art, but the distributed intelligence of the web-connected world transcends art.  Thus, there is a strong, dark, rich requirement for a protocol designed espressoly for the brewing of coffee. Coffee is brewed using coffee pots.  Networked coffee pots require a control protocol if they are to be controlled.', 'member_id' => 5, 'created_at' => new DateTime, 'updated_at' => new DateTime],
			['id' => 6,'subject' => 'Realization', 'body' => 'I was standing in the park wondering why frisbees got bigger as they get closer. Then it hit me.', 'member_id' => 4, 'created_at' => new DateTime, 'updated_at' => new DateTime],
			);

		DB::table('posts')->insert($posts);

	}

}

