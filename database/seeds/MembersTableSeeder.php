<?php

use Illuminate\Database\Seeder;

class MembersTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('members')->delete();

		$members = array(
			['id' => 1,'first_name' => 'Test', 'last_name' => 'One', 'user_id' => 1 , 'created_at' => new DateTime, 'updated_at' => new DateTime],
			['id' => 2,'first_name' => 'Test', 'last_name' => 'Two', 'user_id' => 2 , 'created_at' => new DateTime, 'updated_at' => new DateTime],
			['id' => 3,'first_name' => 'Test', 'last_name' => 'Three', 'user_id' => 3 , 'created_at' => new DateTime, 'updated_at' => new DateTime],
			['id' => 4,'first_name' => 'Test', 'last_name' => 'Four', 'user_id' => 4 , 'created_at' => new DateTime, 'updated_at' => new DateTime],
			['id' => 5,'first_name' => 'Test', 'last_name' => 'Five', 'user_id' => 5 , 'created_at' => new DateTime, 'updated_at' => new DateTime],
			);

		DB::table('members')->insert($members);

	}

}

